// alert(
//   'We are going to simulate an interactive web page using DOM and fetching data from a server'
// )

console.log(
  `fetch() method in JavaScript is used to send requests in the server and load the received responses in the webpages. The request and response is in JSON format`
)

/*
  Syntax: 
    fetch('url', {option})      
    url
      - this is the address which the req is to be made and source where the response will come from (endpoint)
    options
      - this could be an array of properties that contain the HTTP methods, request body, and headers
*/

// get the post data from the url
fetch('https://jsonplaceholder.typicode.com/posts')
  .then((response) => response.json())
  .then((resultFromResponse) => showPosts(resultFromResponse))

// add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  e.preventDefault()

  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: document.querySelector('#text-title').value,
      body: document.querySelector('#text-body').value,
      userId: 1
    }),
    headers: { 'Content-Type': 'application/json; charset=UTF-8' }
  })
    .then((response) => response.json())
    .then((resultFromResponse) => {
      console.log(resultFromResponse)
      alert(`Add post successfully 🥳💯`)

      document.querySelector('#text-title').value = null
      document.querySelector('#text-body').value = null
    })
})

// showPosts() - use to display each post from JSON placeholder
const showPosts = (posts) => {
  let postEntries = ''

  posts.forEach((post) => {
    postEntries += `
    <div id="post-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button id="edit-btn" onclick="editPost('${post.id}')">Edit</button>
      <button id="delete-btn" onclick="deletePost('${post.id}')">Delete</button>
    </div>`
  })
  document.querySelector('#div-post-entries').innerHTML = postEntries
}

// delete post
const deletePost = (id) => {
  document.querySelector(`#post-title-${id}`).remove()
  document.querySelector(`#post-body-${id}`).remove()
  document.querySelector('#edit-btn').remove()
  document.querySelector('#delete-btn').remove()
}

// Mini-Activity:
// Retrieve a single post from JSON API and print it in the console

fetch('https://jsonplaceholder.typicode.com/posts/69')
  .then((response) => response.json())
  .then((resultFromResponse) => console.log(resultFromResponse))

// edit post
const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML
  let body = document.querySelector(`#post-body-${id}`).innerHTML

  document.querySelector(`#text-edit-id`).value = id
  document.querySelector(`#text-edit-title`).value = title
  document.querySelector(`#text-edit-body`).value = body
  document.querySelector(`#btn-submit-update`).removeAttribute('disabled') // to remove the 'disabled' attribute from the update button
}

// update post
document.querySelector(`#form-edit-post`).addEventListener('submit', (e) => {
  e.preventDefault()

  fetch('https://jsonplaceholder.typicode.com/posts/69', {
    method: 'PUT',
    body: JSON.stringify({
      id: document.querySelector('#text-edit-id').value,
      title: document.querySelector('#text-edit-title').value,
      body: document.querySelector('#text-edit-body').value,
      userId: 1
    }),
    headers: { 'Content-Type': 'application/json; charset=UTF-8' }
  })
    .then((fetchResult) => fetchResult.json())
    .then((resultFromResponse) => {
      console.log(resultFromResponse)
      alert(`Update Successfully!`)

      document.querySelector('#text-edit-id').value = null
      document.querySelector('#text-edit-title').value = null
      document.querySelector('#text-edit-body').value = null
      document.querySelector('#btn-submit-update').value = null
    })
})
